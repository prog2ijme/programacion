from trabajo2 import *

rect1 = Rectangulo(80, 20) #Crea un rectángulo con base=80 y altura=20
tria1 = Triangulo(50, 10)  #Crea un triángulo con base=50 y altura=10
cuad1 = Cuadrado(2)        #Crea un cuadrado con lado=2
cuad2 = Cuadrado(100)	   #Crea otro cuadrado, pero con lado=100
circ1 = Circulo(6)         #Crea un círculo con radio=6

tria1.setColor("rojo")     #Cambia el color del triangulo 1 a “rojo”
cuad2.setColor("verde")    #Cambia el color de cuadrado 2 a “verde”
rect1.setColor("azul")     #Cambia el color del rectángulo a “azul”

print("- El Rectangulo 1 es", rect1.getColor())
print(" y tiene una superficie de", rect1.superficie())
assert rect1.getColor()=='azul', 'color debe ser blanco'
assert rect1.superficie()==1600, 'superficie debe ser 1600'

print("- El Triangulo 1 es", tria1.getColor())
print(" y tiene una superficie de", tria1.superficie())
assert tria1.getColor()=='rojo', 'color debe ser rojo'
assert tria1.superficie()==250, 'superficie debe ser 250'

print("- El Cuadrado 1 es", cuad1.getColor())
print(" y tiene una superficie de", cuad1.superficie())
assert cuad1.getColor()=='blanco', 'color debe ser blanco'
assert cuad1.superficie()==4, 'superficie debe ser 4'

print("- El Cuadrado 2 es", cuad2.getColor())
print(" y tiene una superficie de", cuad2.superficie())
assert cuad2.getColor()=='verde', 'color debe ser verde'
assert cuad2.superficie()==10000, 'superficie debe ser 10000'

print("- El Circulo 1 es", circ1.getColor())
print(" y tiene una superficie de", circ1.superficie())
assert circ1.getColor()=='blanco', 'color debe ser blanco'
assert 113 <= circ1.superficie() <= 113.1, 'superficie debe ser 113.04'
