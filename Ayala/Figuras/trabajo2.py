class Figura:
    color="blanco"
    altura=int()
    base=int()
    def getColor(self):
        return self.color
    def setColor(self,uncolor):
        self.color=uncolor
    def __init__(self):
        self.color='blanco'
class Cuadrado(Figura):
    lado=int()
    def __init__(self,unlado):
        self.lado=unlado
    def superficie(self):
        return self.lado**2     
class Triangulo(Figura):
    def __init__(self, unabase, unaaltura):
        self.base=unabase
        self.altura=unaaltura
    def superficie(self):
        return(self.base*self.altura)/2   
class Circulo(Figura):
    pi=3.1416
    radio=int()
    def __init__(self, unradio):
        self.radio=unradio
    def superficie(self):
        return 3.1416 * self.radio ** 2
class Rectangulo(Figura):
    def __init__(self, unabase, unaaltura):
        self.base=unabase
        self.altura=unaaltura
    def superficie(self):
        return self.base*self.altura
    
    
        

