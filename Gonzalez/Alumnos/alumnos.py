class Curso:
    
    
    def __init__(self):
        self.alumnos=list()
        
    def agregarAlumno(self,unAlumno):
        self.alumnos.append(unAlumno)
            
    def mejor(self):
        traga=None
        notaAlta=0
        for mejor in self.alumnos:
            if mejor.nota>notaAlta:
                notaAlta=mejor.nota
                traga=mejor
        return traga

        
    def aprobados (self):
        aprobados=0
        for ap in self.alumnos:
            if ap.nota>=4:
                aprobados+=1       
        return aprobados
            
    
    def promedio (self):
        NotaProm=0
        ContadorTotal=0
        for a in self.alumnos:
            NotaProm=NotaProm+a.nota
            ContadorTotal=ContadorTotal+1
        return NotaProm/ContadorTotal



class Alumno:
    nombre=str()
    nota=int()
    
    def __init__(self,unAlumno):
        self.nombre=unAlumno
        
    
    def setNota(self,unaNota):
        self.nota=unaNota

    def getNombre(self):
        return self.nombre

    def getNota(self):
        return self.nota
