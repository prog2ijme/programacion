from alumnos import *

miCurso = Curso()

a1 = Alumno('Juan')
a1.setNota(6)
miCurso.agregarAlumno(a1)

a2 = Alumno('Pedro')
a2.setNota(10)
miCurso.agregarAlumno(a2)

a3 = Alumno('Luis')
a3.setNota(2)
miCurso.agregarAlumno(a3)

print('El promedio del curso es', miCurso.promedio())
assert (miCurso.promedio()==6), 'promedio debe ser 6'

print('Hay', miCurso.aprobados(),'aprobados')
assert (miCurso.aprobados()==2), 'cant. aprobados debe ser 2'

am = miCurso.mejor()
print('El mejor es',am.getNombre(), 'con', am.getNota())
assert (am.getNombre()=='Pedro'), 'debe ser Pedro'
assert (am.getNota()==10), 'la nota debe ser 10'
