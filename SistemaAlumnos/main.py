import sys, os
from PyQt4.QtGui import QMainWindow, QApplication, QMessageBox, QTextEdit, QPrintPreviewDialog
from PyQt4 import Qsci, QtCore, QtGui, uic

from alumnos.frmalumno import FrmAlumno
from materias.frmmateria import FrmMateria
from notas.frmnotas import FrmNotas
from profesores.frmprofesor import FrmProfesor

from modelo.instituto import Instituto

class App(QApplication):
    nombre = 'Sistema de Alumnos'
    autor = 'Prog2 IJME - 2014'
    
    def __init__(self, args):
        super().__init__(args, True)
        self.ventana = QMainWindow()
        self.instituto = Instituto()
        self.ventanaAlumnos = FrmAlumno(self)
        self.ventanaMaterias = FrmMateria(self)
        self.ventanaNotas = FrmNotas(self)
        self.ventanaProfesores = FrmProfesor(self)
        self.path = os.path.dirname(os.path.realpath(args[0]))
        self.ui = uic.loadUi(self.path+"/main.ui", self.ventana)
        
    def iniciar(self):
        self.conectarEventos()
        self.ventana.show()
        status = self.exec_()
        sys.exit(status)
        
    def conectarEventos(self):
        self.lastWindowClosed.connect(self.quit)
        self.ui.actionAcerca_de.triggered.connect(self.acercaDe)
        self.ui.btnAlumnos.clicked.connect(self.abrirAlumnos)
        self.ui.btnMaterias.clicked.connect(self.abrirMaterias)
        self.ui.btnNotas.clicked.connect(self.abrirNotas)
        self.ui.btnProfesores.clicked.connect(self.abrirProfesores)
        self.ui.btnPromedios.clicked.connect(self.abrirPromedio)
        
    def abrirAlumnos(self):
        self.ventanaAlumnos.iniciar()

    def abrirMaterias(self):
        self.ventanaMaterias.iniciar()

    def abrirProfesores(self):
        self.ventanaProfesores.iniciar()
        
    def abrirNotas(self):
        self.ventanaNotas.iniciar()
        
    def abrirPromedio(self):
        text = '''
        <html>
          <body>
          <center>
            <h2>Promedios de Alumnos</h2>
            <br/>
            <table>
              <tr><th> Legajo </th><th> Nombre </th><th> Promedio </th></tr>
              <tr>
        '''
        text += '</tr><tr>'.join(['<td>' + str(a.legajo) + ' </td><td> ' + a.nombre + ' </td><td> ? </td>' for a in self.instituto.alumnos])
        text += '''
              </tr>
            </table>
            </center>
          </body>
        </html>
        '''
        dialog = QPrintPreviewDialog()
        editor = QTextEdit(self.ventana)
        editor.setHtml(text)
        dialog.paintRequested.connect(editor.print_)
        dialog.exec_()
        
    def acercaDe(self):
        QMessageBox.about(self.ventana, 'Acerca de... '+self.nombre, 'Desarrollado por... '+self.autor) 

def main(args):
    app = App(args)
    app.iniciar()
   
if __name__ == '__main__':
    main(sys.argv)
