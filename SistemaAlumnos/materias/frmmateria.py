import sys, os
from PyQt4.QtGui import QDialog
from PyQt4 import Qsci, QtCore, QtGui, uic

class FrmMateria:
    
    def __init__(self, app):
        self.ventana = QDialog(app.ventana)
        self.instituto = app.instituto
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.ui = uic.loadUi(self.path+"/frmmateria.ui", self.ventana)
        
    def iniciar(self):
        self.reset()
        self.cargarComboProfesores()
        self.conectarEventos()
        self.ventana.show()
        
    def reset(self):
        self.ui.cmbProfesores.clear()
        for child in self.ui.findChildren(QtGui.QLineEdit):
            child.clear()
            
    def conectarEventos(self):
        self.ui.buttonBox.accepted.connect(self.agregarMateria)
        
    def agregarMateria(self):
        codigo = self.ui.txtCodigo.text()
        nombre = self.ui.txtNombre.text()
        descripcion = self.ui.txtDescripcion.text()

        if self.ui.rad1.isChecked():
            anio= 1
        elif self.ui.rad2.isChecked():
            anio= 2
        elif self.ui.rad3.isChecked():
            anio= 3

        index = self.ui.cmbProfesores.currentIndex() #obtiene la posicion seleccionada del combo
        profesor = self.ui.cmbProfesores.itemData(index)  #obtiene el dato guardado en esa posicion
        self.instituto.agregarMateria(codigo, nombre, descripcion, anio)
        
    def cargarComboProfesores(self):
        for p in self.instituto.getProfesores(): #recorre todos los profesores del instituto
            self.ui.cmbProfesores.addItem(p.cuil+' - '+p.nombre, p) #agrega cada uno al combo (muestra el nombre)


     

        
             
