import sys, os
from PyQt4.QtGui import QDialog
from PyQt4 import Qsci, QtCore, QtGui, uic

class FrmAlumno:
    
    def __init__(self, app):
        self.ventana = QDialog(app.ventana)
        self.instituto = app.instituto
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.ui = uic.loadUi(self.path+"/frmalumno.ui", self.ventana)
        
    def iniciar(self):
        self.reset()
        self.conectarEventos()
        self.ventana.show()
        
    def reset(self):
        for child in self.ui.findChildren(QtGui.QLineEdit):
            child.clear()
            
    def conectarEventos(self):
        self.ui.buttonBox.accepted.connect(self.agregarAlumno)

    def agregarAlumno(self):
        legajo = self.ui.txtLegajo.text()
        nombre = self.ui.txtNombre.text()
        self.instituto.agregarAlumno(legajo,nombre)
        
