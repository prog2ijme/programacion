import os, sqlite3

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

class BaseDeDatos:
	def conectar(self):
		self.path = os.path.dirname(os.path.realpath(__file__))
		self.db = sqlite3.connect('alumnos.dat')
		self.db.row_factory = dict_factory
		self.cursor = self.db.cursor()
	
	def desconectar(self):
		self.db.close()
		
	def ejecutar(self, sql):
                self.conectar()
                self.cursor.execute(sql)
                res = self.cursor.fetchall()
                self.db.commit()
                self.desconectar()
                return res
                
	def insertar(self, tabla, campos, valores):
		sql = "INSERT INTO "
		sql+= tabla + "(" +  ",".join(campos) + ") "
		sql+= "VALUES ('" +  "','".join([str(v) for v in valores]) + "');"
		self.ejecutar(sql)		
		
	def actualizar(self, tabla, ident, campos, valores):
		sql = "UPDATE " + tabla + " SET "
		sql+= ",".join([ c + " = '" + str(v) + "'" for (c,v) in zip(campos, valores)]) + " "
		sql+= "WHERE id = " + str(ident) + ";"
		self.ejecutar(sql)		

	def buscar(self, tabla, campos=['*'], condicion=''):
		sql = "SELECT " +  ",".join(campos) + " "
		sql+= "FROM " + tabla + " "
		sql+= "WHERE " + condicion + ";" if condicion != '' else ';'
		return self.ejecutar(sql)
		
                
