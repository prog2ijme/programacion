from modelo.alumno import Alumno
from modelo.materia import Materia
from modelo.profesor import Profesor

class Instituto:

    def __init__(self):
        self.alumnos = list() #self.getListaAlumnos()
        self.materias = list()
        self.profesores = list()

    def agregarAlumno(self, unLegajo, unNombre, unApellido, unTelefono, unSexo, unaEdad, unaMate):
       unAlumno = Alumno(unLegajo, unNombre, unApellido, unTelefono, unSexo, unaEdad, unaMate)
       self.alumnos.append(unAlumno)
       unAlumno.guardar()

    def agregarMateria(self, unCod, unNombre, unaDesc, unAnio):
        unaMateria = Materia(unCod, unNombre, unaDesc, unAnio)
        self.materias.append(unaMateria)
        unaMateria.guardar()

    def agregarProfesor(self, unNombre, unApellido, unCuil, unaDireccion):
        unProfesor = Profesor(unNombre, unApellido, unCuil, unaDireccion)
        self.profesores.append(unProfesor)
        unProfesor.guardar()
        
    def getListaAlumnos(self):
        return Alumno.getTodos()

    def getMaterias(self):
        return self.materias

    def getProfesores(self):
        return self.profesores

