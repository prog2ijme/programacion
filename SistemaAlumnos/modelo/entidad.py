from modelo.base import BaseDeDatos

class Entidad:
    table = None
    fields = []
    
    def guardar(self):
        db = BaseDeDatos()
        if self.id is None:
            db.insertar(self.tabla, self.campos, [getattr(self,c) for c in self.campos])
        else:
            db.actualizar(self.tabla, self.id, self.campos, [getattr(self,c) for c in self.campos])

    @classmethod
    def getTodos(cls):
        return cls.buscar()

    @classmethod
    def buscar(cls, filtro=''):
        db = BaseDeDatos()
        return [cls(**r) for r in db.buscar(cls.tabla, condicion=filtro)]   
