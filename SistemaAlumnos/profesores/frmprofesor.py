import sys, os
from PyQt4.QtGui import QDialog
from PyQt4 import Qsci, QtCore, QtGui, uic

class FrmProfesor:
    
    def __init__(self, app):
        self.ventana = QDialog(app.ventana)
        self.instituto = app.instituto
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.ui = uic.loadUi(self.path+"/frmprofesor.ui", self.ventana)
        
    def iniciar(self):
        self.reset()
        self.conectarEventos()
        self.ventana.show()
        
    def reset(self):
        for child in self.ui.findChildren(QtGui.QLineEdit):
            child.clear()
            
    def conectarEventos(self):
        self.ui.buttonBox.accepted.connect(self.agregarProfesor)

    def agregarProfesor(self):
        nombre = self.ui.txtNombre.text()
        apellido = self.ui.txtApellido.text()
        cuil = self.ui.txtCuil.text()
        direccion = self.ui.txtDireccion.text()
        self.instituto.agregarProfesor(nombre, apellido, cuil, direccion)

    def mostrarProfesor(self):
        for p in self.instituto.profesores:
            print (p)

    def buscarProfesor(self, unCuil):
        for p in self.instituto.profesores:
            if p.cuil == unCuil:
                print (p)
            else:
                print ('no se encuentra profesor')
            
