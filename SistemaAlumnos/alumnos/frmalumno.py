import sys, os
from PyQt4.QtGui import QDialog, QTableWidgetItem
from PyQt4.QtCore import QDate
from PyQt4 import Qsci, QtCore, QtGui, uic

class FrmAlumno:
    
    def __init__(self, app):
        self.ventana = QDialog(app.ventana)
        self.instituto = app.instituto
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.ui = uic.loadUi(self.path+"/frmalumno.ui", self.ventana)
        self.materiasSeleccionadas = []
        
    def iniciar(self):
        self.cargarMaterias()
        self.reset()
        self.conectarEventos()
        self.ventana.show()
        
    def reset(self):
        for child in self.ui.findChildren(QtGui.QLineEdit):
            child.clear()
            
    def conectarEventos(self):
        self.ui.guardar.clicked.connect(self.agregarAlumno)
        self.ui.fechaNac.dateChanged.connect (self.mostrarFecha)
        self.ui.cmbAnios.currentIndexChanged.connect(self.seleccionAnio)
        self.ui.btnAgregarMateria.clicked.connect(self.agregarMateria)
        self.ui.btnQuitarMateria.clicked.connect(self.quitarMateria)

    def agregarAlumno(self):
        legajo = self.ui.txtLegajo.text()
        nombre = self.ui.txtNombre.text()
        apellido = self.ui.txtApellido.text()
        telefono = self.ui.txtTelefono.text()
        sexo = self.ui.cmbSexo.currentIndex()        
        edad = self.mostrarFecha()
        materia = self.materiasSeleccionadas
        self.instituto.agregarAlumno(legajo, nombre, apellido, telefono, sexo, edad, materia)

    def mostrarFecha(self):
        fechaNac = self.ui.fechaNac.date()
        diaNac = fechaNac.day()
        mesNac = fechaNac.month()
        anioNac = fechaNac.year()
        
        hoy = QDate.currentDate()
        diaHoy = hoy.day()
        mesHoy = hoy.month()
        anioHoy = hoy.year()
        
        if mesNac < mesHoy or (mesNac==mesHoy and diaNac <= diaHoy):
            edad = anioHoy - anioNac
        else:
            edad = (anioHoy - anioNac)- 1
        self.ui.lblEdad.setText(str(edad)+' años')
        return edad

    def seleccionAnio(self, index):
        self.cargarMaterias(index+1)

    def cargarMaterias(self, anio=0):
        self.ui.cmbNombreMaterias.clear()
        for m in self.instituto.getMaterias():
            if anio == 0 or m.anio == anio:
                self.ui.cmbNombreMaterias.addItem(m.codigo+' - '+m.nombre, m)

    def agregarMateria(self):
        index = self.ui.cmbNombreMaterias.currentIndex()
        materia = self.ui.cmbNombreMaterias.itemData(index)
        if not materia in self.materiasSeleccionadas:
            self.materiasSeleccionadas.append(materia)
            self.insertarMateriaEnTabla(materia)

    def insertarMateriaEnTabla(self, materia):
        newRow = self.ui.tablaMaterias.rowCount()
        self.ui.tablaMaterias.insertRow(newRow)
        item0 = QTableWidgetItem(str(materia.anio))
        item0.setData(1, materia)
        self.ui.tablaMaterias.setItem(newRow, 0, item0)
        item1 = QTableWidgetItem(materia.codigo)
        self.ui.tablaMaterias.setItem(newRow, 1, item1)
        item2 = QTableWidgetItem(materia.nombre)
        self.ui.tablaMaterias.setItem(newRow, 2, item2)

    def quitarMateria(self):
        row = self.ui.tablaMaterias.currentRow()
        if row >= 0:
            item = self.ui.tablaMaterias.item(row,0)
            materia = item.data(1)
            self.materiasSeleccionadas.remove(materia)
            self.ui.tablaMaterias.removeRow(row)


        
