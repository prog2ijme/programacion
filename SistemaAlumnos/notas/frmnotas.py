import sys, os
from PyQt4.QtGui import QDialog
from PyQt4 import Qsci, QtCore, QtGui, uic

class FrmNotas:

    def __init__(self,app):
        self.ventana=QDialog(app.ventana)
        self.instituto=app.instituto
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.ui = uic.loadUi(self.path+"/frmnotas.ui", self.ventana)

    def iniciar(self):
        #self.reset()
        self.conectarEventos()
        self.ventana.show()

    def conectarEventos(self):
        self.ui.buttonBox.accepted.connect(self.agregarNota)

    def agregarNota(self):
        nombre=self.ui.txtNombre.text()
        nota=self.ui.txtNota.text()
        self.instituto.agregarNota(nombre,nota)

