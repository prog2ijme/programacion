class Figura:
    color=str()
    superficie=int()
    def __init__(self):
        self.color="blanco"
    def setColor(self,unColor):
        self.color= unColor
    def getColor(self):
        return self.color

class Cuadrado(Figura):
    def __init__(self,unLado=0,unColor="blanco"):
        self.lado1=unLado
        self.lado2=unLado
        self.color=unColor
    def superficie (self):
        return self.lado1*self.lado2

class Triangulo(Figura):
    def __init__(self,unaBase=0,unaAltura=0,unColor="blanco"):
        self.base=unaBase
        self.altura=unaAltura
        self.color=unColor
    def superficie(self):
        return self.base*self.altura/2

class Circulo(Figura):
    def __init__(self,Pi=3.14,unRadio=0,unColor="blanco"):
        self.Pi=Pi
        self.radio=unRadio
        self.color=unColor
    def superficie(self):
        return self.Pi*self.radio^2

class Rectangulo(Figura):
    base=int()
    altura=int()
    def __init__(self,unaBase=0,unaAltura=0,unColor="blanco"):
        self.base=unaBase
        self.altura=unaAltura
        self.color=unColor
    def superficie(self):
        return self.base*self.altura
