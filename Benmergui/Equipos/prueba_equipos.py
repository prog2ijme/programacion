from equipos import *

arg = Equipo('Argentina')
bra = Equipo('Brasil')
ale = Equipo('Alemania')

p1 = Partido(local=bra, visitante=ale)
p1.jugar(2, 1)
print('Ganó',p1.ganador().getNombre())
assert (p1.ganador().getNombre()=='Brasil'), 'debe ser Brasil'

p2 = Partido(local=ale, visitante=arg)
p2.jugar(0, 1)
print('Ganó',p2.ganador().getNombre())
assert (p2.ganador().getNombre()=='Argentina'), 'debe ser Argentina'

p3 = Partido(local=arg, visitante=bra)
p3.jugar(3, 2)
print('Ganó',p3.ganador().getNombre())
assert (p3.ganador().getNombre()=='Argentina'), 'debe ser Argentina'

print(arg.getNombre(),'tiene', arg.getPuntos(),', hizo',arg.getGoles(),'goles')
assert (arg.getPuntos()==6), 'puntos debe ser 6'
assert (arg.getGoles()==4), 'goles debe ser 4'

print(bra.getNombre(),'tiene', bra. getPuntos(),', hizo', bra.getGoles(),'goles')
assert (bra.getPuntos()==3), 'puntos debe ser 3'
assert (bra.getGoles()==4), 'goles debe ser 4'

print(ale.getNombre(),'tiene',ale. getPuntos(),', hizo', ale.getGoles(),'goles')
assert (ale.getPuntos()==0), 'puntos debe ser 0'
assert (ale.getGoles()==1), 'goles debe ser 1'
