import sys
import os
from gettext import textdomain, bindtextdomain, gettext as _
from tkinter import Tk, Menu, Toplevel, Text, PhotoImage, Canvas
from tkinter import Frame, Label, Scrollbar, PanedWindow, Button as tkButton
from tkinter import Pack, Grid, Place, IntVar
from tkinter.ttk import Style, Frame, Label, Scrollbar, Button  # PanedWindow
from tkinter.constants import INSERT, LAST, END, VERTICAL, HORIZONTAL, RIDGE
from tkinter.constants import RIGHT, LEFT, BOTTOM, X, Y, BOTH, NONE
from tkinter.filedialog import askopenfilename, asksaveasfilename
from tkinter.messagebox import showerror, showinfo
from sqlite3 import connect, Row
import webbrowser


class ScrolledText(Text):
    """Modified version of tkinter.scrolledtext width horizontal scrollbar"""
    def __init__(self, master=None, **kw):
        self.frame = Frame(master)
        self.vbar = Scrollbar(self.frame)
        self.vbar.pack(side=RIGHT, fill=Y)
        #>>>MNC: For horizontal scrollbar
        try:
            self.hbar = None
            if kw['wrap'] == NONE:
                self.hbar = Scrollbar(self.frame, orient=HORIZONTAL)
                self.hbar.pack(side=BOTTOM, fill=X)
                kw.update({'xscrollcommand': self.hbar.set})
        except KeyError:
            self.hbar = None
        #<<<MNC
        kw.update({'yscrollcommand': self.vbar.set})
        Text.__init__(self, self.frame, **kw)
        self.pack(side=LEFT, fill=BOTH, expand=True)
        self.vbar['command'] = self.yview
        #>>>MNC: For horizontal scrollbar
        if self.hbar is not None:
            self.hbar['command'] = self.xview
        #<<<MNC
        # Copy geometry methods of self.frame without overriding Text
        # methods -- hack!
        text_meths = vars(Text).keys()
        methods = vars(Pack).keys() | vars(Grid).keys() | vars(Place).keys()
        methods = methods.difference(text_meths)
        for m in methods:
            if m[0] != '_' and m != 'config' and m != 'configure':
                setattr(self, m, getattr(self.frame, m))

    def __str__(self):
        return str(self.frame)

    def setText(self, text):
        self.delete(1.0, END)
        self.insert(END, text)

    def insertText(self, text):
        return self.insert(INSERT, text)

    def getText(self):
        return self.get(1.0, END)


class TextResult(ScrolledText):
    """Scrolled Text not editable"""
    def __init__(self, root):
        super().__init__(root, wrap=NONE)
        self.configure(state='disabled')

    def setText(self, text):
        self.configure(state='normal')
        super().setText(text)
        self.configure(state='disabled')


class CodeText(ScrolledText):
    """Scrolled Text sintax highlighting
    (Keywords, Builtins, Strings, Comments, Tags)"""
    IGNORE_CASE = False
    KEYWORDS = []
    BUILTINS = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._tags = {
            "1-keyword": {"color": "#0000DD", "patterns": {}},
            "2-builtins": {"color": "#900090", "patterns": {}},
            "3-string": {"color": "#008800", "patterns": {}},
            "4-comment": {"color": "#FF0000", "patterns": {}},
            "5-tag": {"color": "#BBBBBB", "patterns": {}}
        }
        self.config(undo=True)
        self.add_patterns(self.KEYWORDS, "1-keyword", keyword=True)
        self.add_patterns(self.BUILTINS, "2-builtins", keyword=True)
        self.add_patterns([r"(?s)'[^']*'", r'(?s)"[^"]*"'], '3-string')
        self.add_pattern(r'(?s)/\*(.(?!/\*))*\*/', '4-comment')
        self.add_pattern(r'(?s)<[^><]*>', '5-tag')
        self.bind("<KeyRelease>", lambda e: self.highlight())
        self.bind("<Control-y>",
            lambda e: 'break' if self.edit_redo() else 'break')
        self.bind("<Tab>",
            lambda e: 'break' if self.insert(INSERT, ' ' * 4) else 'break')

    def add_patterns(self, patterns, tag, keyword=False):
        for pattern in patterns:
            self.add_pattern(pattern, tag, keyword)

    def add_pattern(self, pattern, tag, keyword=False):
        if keyword:
            ci = '(?i)' if self.IGNORE_CASE else ''
            pattern = ci + "[[:<:]]" + pattern + "[[:>:]]"
        self._tags[tag]['patterns'][pattern] = tag

    def highlight(self):
        for tag in sorted(self._tags.keys()):
            self.tag_delete(tag)
            self.tag_configure(tag, foreground=self._tags[tag]['color'])
            for pattern in self._tags[tag]['patterns']:
                self.highlight_pattern(pattern, tag, regexp=True)

    def setText(self, text):
        super().setText(text)
        self.highlight()

    def highlight_pattern(self, pattern, tag, start="1.0", end="end",
                            regexp=False):
        '''Apply the given tag to all text that matches the given pattern
        If 'regexp' is set to True, pattern will be treated as a
        regular expression
        '''
        start = self.index(start)
        end = self.index(end)
        self.mark_set("matchStart", start)
        self.mark_set("matchEnd", start)
        self.mark_set("searchLimit", end)
        count = IntVar()
        count.set(0)
        while True:
            index = self.search(pattern, "matchEnd", "searchLimit",
                count=count, regexp=regexp)
            if index == "" or count.get() == 0:
                break
            else:
                end = "{0}+{1}c".format(index, count.get())
                self.mark_set("matchStart", index)
                self.mark_set("matchEnd", end)
                self.tag_add(tag, "matchStart", "matchEnd")

import keyword
class TextPython(CodeText):
    IGNORE_CASE = False
    KEYWORDS = keyword.kwlist
    BUILTINS = dir (__builtins__)

class TextSQL(CodeText):
    """Text SQL sintax highlighting"""
    IGNORE_CASE = True
    KEYWORDS = [
        'ABORT', 'ACTION', 'ADD', 'AFTER', 'ALL', 'ALTER', 'ANALYZE', 'AND',
        'AS', 'ASC', 'ATTACH', 'AUTOINCREMENT', 'BEFORE', 'BEGIN', 'BETWEEN',
        'BLOB', 'BY', 'CASCADE', 'CASE', 'CAST', 'CHECK', 'COLLATE', 'COLUMN',
        'COMMIT', 'CONFLICT', 'CONSTRAINT', 'CREATE', 'CROSS', 'CURRENT_DATE',
        'CURRENT_TIME', 'CURRENT_TIMESTAMP', 'DATABASE', 'DEFAULT',
        'DEFERRABLE', 'DEFERRED', 'DELETE', 'DESC', 'DETACH', 'DISTINCT',
        'DROP', 'EACH', 'ELSE', 'END', 'ESCAPE', 'EXCEPT', 'EXCLUSIVE',
        'EXISTS', 'EXPLAIN', 'FAIL', 'FOR', 'FOREIGN', 'FROM', 'FULL', 'GLOB',
        'GROUP', 'HAVING', 'IF', 'IGNORE', 'IMMEDIATE', 'IN', 'INDEX',
        'INDEXED', 'INITIALLY', 'INNER', 'INSERT', 'INSTEAD', 'INTEGER',
        'INTERSECT', 'INTO', 'IS', 'ISNULL', 'JOIN', 'KEY', 'LEFT', 'LIKE',
        'LIMIT', 'MATCH', 'NATURAL', 'NO', 'NOT', 'NOTNULL', 'NULL', 'OF',
        'OFFSET', 'ON', 'OR', 'ORDER', 'OUTER', 'PLAN', 'PRAGMA', 'PRIMARY',
        'QUERY', 'RAISE', 'REAL', 'RECURSIVE', 'REFERENCES', 'REGEXP',
        'REINDEX', 'RELEASE', 'RENAME', 'REPLACE', 'RESTRICT', 'RIGHT',
        'ROLLBACK', 'ROW', 'SAVEPOINT', 'SELECT', 'SET', 'TABLE', 'TEMP',
        'TEMPORARY', 'TEXT', 'THEN', 'TO', 'TRANSACTION', 'TRIGGER', 'UNION',
        'UNIQUE', 'UPDATE', 'USING', 'VACUUM', 'VALUES', 'VIEW', 'VIRTUAL',
        'WHEN', 'WHERE', 'WITH', 'WITHOUT'
    ]

    BUILTINS = [
        # Aggregate Functions
        'avg', 'count', 'group_concat', 'max', 'min', 'sum', 'total',
        # Date And Time Functions
        'date', 'time', 'datetime', 'julianday', 'strftime',
        # Core Functions
        'abs', 'changes', 'char', 'coalesce', 'glob', 'ifnull', 'instr', 'hex',
        'last_insert_rowid', 'length', 'like', 'likelihood', 'likely',
        'load_extension', 'lower', 'ltrim', 'max', 'min', 'nullif', 'printf',
        'quote', 'random', 'randomblob', 'replace', 'round', 'rtrim', 'soundex',
        'sqlite_compileoption_get', 'sqlite_compileoption_used',
        'sqlite_source_id', 'sqlite_version', 'substr', 'total_changes', 'trim',
        'typeof', 'unlikely', 'unicode', 'upper', 'zeroblob'
    ]


class DiagramCanvas(Canvas):

    def __init__(self, parent, width=800, height=600):
        super(DiagramCanvas, self).__init__(parent)
        self.config(width=width, height=height, bg='#F9F6EA')
        self.width = width
        self.height = height
        self.items = {}
        self.arrows = {}
        self.blocks = {}
        self.margin = 20
        self.spacing = 80
        self._drag_data = {}
        self.tag_bind("block", "<ButtonPress-1>", self.onDragStart)
        self.tag_bind("block", "<ButtonRelease-1>", self.onDragEnd)
        self.tag_bind("block", "<B1-Motion>", self.onDrag)

    def bestPlace(self, b):
        c = self.width / 2
        m = self.height / 2
        bw = b.width
        bh = b.height
        sp = self.spacing
        places = [
            (c, m),
            (c, m - (bh + sp)),
            (c - (bw + sp), m - (bh + sp)),
            (c - (bw + sp), m),
            (c + (bw + sp), m - (bh + sp)),
            (c + (bw + sp), m),
            (c, m + (bh + sp)),
            (c - (bw + sp), m + (bh + sp)),
            (c + (bw + sp), m + (bh + sp))
        ]
        if len(self.blocks) < len(places):
            return places[len(self.blocks)]
        else:
            return places[0]

    def addBlock(self, text, x=0, y=0):
        b = Block(text, x, y)
        if (x, y) == (0, 0):
            (x, y) = self.bestPlace(b)
            b.x = x
            b.y = y
        b.id = self.create_rectangle(b.left(), b.top(), b.right(), b.bottom(),
            outline=b.borderColor, fill=b.color, tags="block")
        b.textid = self.create_text(b.x, b.y, text=b.text,
            tags="block", font=b.font)
        self.items[b.id] = b
        self.items[b.textid] = b
        self.blocks[text] = b
        return b

    def addArrow(self, blockFrom, blockTo, where=None):
        where1, where2 = where if where is not None else (None, None)
        x1, y1 = blockFrom.faceTo(blockTo, where1)
        x2, y2 = blockTo.faceTo(blockFrom, where2)
        shape = (16, 16, 10)
        idArrow = self.create_line(x1, y1, x2, y2, arrow=LAST, arrowshape=shape)
        self.arrows[idArrow] = (blockFrom, blockTo, where1, where2)

    def onDragEnd(self, event):
        '''End drag of an object'''
        # reset the drag information
        self._drag_data["item"] = None
        self._drag_data["x"] = 0
        self._drag_data["y"] = 0

    def onDrag(self, event):
        '''Handle dragging of an object'''
        # compute how much this object has moved
        delta_x = event.x - self._drag_data["x"]
        delta_y = event.y - self._drag_data["y"]
        # move the object the appropriate amount
        item = self._drag_data["item"]
        item.x += delta_x
        item.y += delta_y
        for i in item.ids():
            self.move(i, delta_x, delta_y)
        # record the new position
        self._drag_data["x"] = event.x
        self._drag_data["y"] = event.y
        for idArrow in self.arrows:
            b1, b2, where1, where2 = self.arrows[idArrow]
            x1, y1 = b1.faceTo(b2, where1)
            x2, y2 = b2.faceTo(b1, where2)
            self.coords(idArrow, x1, y1, x2, y2)

    def onDragStart(self, event):
        '''Being drag of an object'''
        # record the item and its location
        idItem = self.find_closest(event.x, event.y)[0]
        self._drag_data["item"] = self.items[idItem]
        self._drag_data["x"] = event.x
        self._drag_data["y"] = event.y


class Block:

    def __init__(self, text, x=0, y=0, color="#F5D3AC", padx=20, pady=5):
        self.id = None
        self.textid = None
        self.text = text
        self.color = color
        self.width = len(text) * 10 + 2 * padx
        self.height = 18 + 2 * pady
        self.x = x
        self.y = y
        self.jointo = []
        self.font = "Courier 10 bold"
        self.borderColor = "black"

    def ids(self):
        return [self.id, self.textid]

    def left(self):
        return self.x - (self.width / 2)

    def top(self):
        return self.y - (self.height / 2)

    def right(self):
        return self.x + (self.width / 2)

    def bottom(self):
        return self.y + (self.height / 2)

    def pos(self, where):
        hor, ver = where
        x = self.right() if hor == 'r' else (
            self.left() if hor == 'l' else self.x)
        y = self.top() if ver == 't' else (
            self.bottom() if ver == 'b' else self.y)
        return (x, y)

    def faceTo(self, otherBlock, where=None):
        if where is not None:
            return self.pos(where)
        else:
            y = 't' if self.top() > otherBlock.bottom() else (
                'b' if self.bottom() < otherBlock.top() else 'm')
            x = 'c' if y != 'm' else (
                'l' if self.left() > otherBlock.right() else (
                'r' if self.right() < otherBlock.left() else 'c'))
            return self.pos(x + y)


class UIBuilder:

    def __init__(self, parent=None, ui_config=None, commands=None):
        self.ui = {}
        self.ui_config = ui_config
        self.win = parent if parent is not None else Tk()
        try:
            self.path = os.path.realpath(__file__)
        except NameError:
            self.path = os.path.realpath('__file__')
        self.dir = os.path.dirname(self.path) + '/'
        if self.ui_config is not None and 'title' in self.ui_config:
            self.win.title(self.ui_config['title'])
            iconPath = self.dir + self.ui_config['icon']
            icon = PhotoImage(master=self.win, file=iconPath)
            self.win.tk.call('wm', 'iconphoto', self.win._w, icon)
            self.win.geometry(self.ui_config['size'])
        if commands is not None:
            self.bindCommands(commands)
        self.configureLocale()

    def bindCommands(self, commands):
        self.commands = commands.copy()
        self.commands.update({'THEME': self.changeTheme})
        self.commands.update({'EXIT': self.exitApp})

    def configureLocale(self):
        if sys.platform.startswith('win'):
            import locale
            if os.getenv('LANG') is None:
                lang, enc = locale.getdefaultlocale()
                os.environ['LANG'] = lang
        textdomain("app")
        bindtextdomain("app", "./locale")

    def start(self):
        self.win.mainloop()

    def createMainMenu(self, config):
        self.menu = Menu(self.win)
        self.win.config(menu=self.menu)
        self.createMenu(self.menu, config)

    def createMenu(self, parent, menuconfig):
        for i in menuconfig:
            self.createMenuItem(parent, i)

    def createMenuItem(self, menu, item):
        if item['label'] == '-':
            menu.add_separator()
        elif 'items' in item:
            sm = Menu(menu, tearoff=0)
            menu.add_cascade(label=_(item['label']), underline=0, menu=sm)
            self.ui[item['id']] = sm
            for i in item['items']:
                self.createMenuItem(sm, i)
        elif 'cmd' in item:
            key = '' if 'key' not in item else item['key']
            args = {} if 'args' not in item else item['args']
            menu.add_command(label=_(item['label']),
                underline=0,
                command=self.command(cmd=item['cmd'], **args),
                accelerator=key)

    def createContent(self, parent=None, config=None):
        if config is None:
            self.createMainContent()
        else:
            if parent is None:
                parent = self.win
            for item in config:
                self.createContentItem(parent, item)
        return self.ui

    def createMainContent(self):
        if 'menu' in self.ui_config:
            self.createMainMenu(self.ui_config['menu'])
        if 'content' in self.ui_config:
            self.createContent(self.win, self.ui_config['content'])

    def createContentItem(self, parent, item):
        el = item['type'](parent)
        self.ui[item['id']] = el
        if 'text' in item:
            el.config(text=_(item['text']))
        if 'signal' in item:
            signal = item['signal']
            args = {} if 'args' not in signal else signal['args']
            event = '<' + signal['event'] + '>'
            el.bind(event, self.eventCommand(cmd=signal['cmd'], **args))
        if 'cmd' in item:
            args = {} if 'args' not in item else item['args']
            el.config(command=self.command(cmd=item['cmd'], **args))
        if 'dimension' in item:
            w, h = item['dimension']
            el.config(width=w, height=h)
        if 'cfg' in item:
            el.config(**item['cfg'])
        if parent.__class__ == PanedWindow:
            parent.add(el)
        elif 'side' in item:
            el.pack(side=item['side'], pady=10, padx=10, fill="both",
            expand=('expand' in item and item['expand']))
        elif 'grid' in item:
            r, c = item['grid']
            el.grid(row=r, column=c, padx=3, pady=3)
        if 'items' in item:
            for i in item['items']:
                self.createContentItem(el, i)

    def eventCommand(self, cmd, **args):
        return lambda e: self.onCommand(cmd, **args)

    def command(self, cmd, **args):
        return lambda: self.onCommand(cmd, **args)

    def onCommand(self, cmd, **args):
        try:
            self.commands[cmd](**args)
        except KeyError:
            showerror(_('Error'), _('Unknown command: ') + cmd)

    def changeTheme(self, name):
        self.win.style.theme_use(name)

    def exitApp(self):
        self.win.destroy()


class QueryBrowserUI:
    appName = "dbEditor"
    author = "Martín Nicolás Carbone"
    configFile = '.dbeditor.conf'
    sqlSqliteUrl = "http://www.sqlite.org/lang.html"
    tablesAtLeft = True
    selectedTable = None
    dbOpened = False

    def __init__(self, app):
        self.win = Tk()
        self.win.style = Style()
        themes = self.win.style.theme_names()
        themes_menu = [{
                'label':t,
                'cmd': 'THEME',
                'args':{'name':t}
        } for t in themes]
        self.uiCfg = {
                'title': 'DBEditor',
                'icon': 'icon.gif',
                'size': '900x600',
        ## Main Menu -----------------------------------------------------
                'menu': [{
                    'id': 'FILE',
                    'label':'File',
                    'items': [{
                        'label':'New...',
                        'cmd': 'NEW'
                    }, {
                        'label':'Open...',
                        'cmd': 'OPEN'
                    }, {
                        'label':'-'
                    }, {
                        'label':'Quit',
                        'cmd': 'EXIT'
                    }]
                }, {
                    'id': 'VIEW',
                    'label':'View',
                    'items': [{
                        'label': 'Tables at right',
                        'cmd': 'TABLES_RL'
                    }, {
                        'id': 'THEME',
                        'label':'Theme',
                        'items': themes_menu
                    }, {
                        'label':'ERD...',
                        'cmd': 'ERD'
                    }, {
                        'label':'Source code...',
                        'cmd': 'SOURCE'
                    }]
                }, {
                    'id': 'SQL',
                    'label':'SQL',
                    'items': [{
                        'id': 'DDL',
                        'label': 'DDL',
                        'items': [{
                            'label': 'CREATE TABLE',
                            'cmd': 'QUERY',
                            'args':{'query': 'CREATE', 'table':None}
                        }, {
                            'label': 'ALTER ... ADD COLUMN',
                            'cmd': 'QUERY',
                            'args':{'query': 'ADD'}
                        }, {
                            'label': 'ALTER ... RENAME',
                            'cmd': 'QUERY',
                            'args':{'query': 'RENAME'}
                        }, {
                            'label': 'DROP',
                            'cmd': 'QUERY',
                            'args':{'query': 'DROP'}
                        }, {
                            'label': 'CREATE VIEW',
                            'cmd': 'QUERY',
                            'args':{'query': 'VIEW'}
                        }, {
                            'label': 'CREATE TRIGGER',
                            'cmd': 'QUERY',
                            'args':{'query': 'TRIGGER'}
                         }]
                    }, {
                        'id': 'DML',
                        'label': 'DML',
                        'items': [{
                            'id': 'SELECT',
                            'label': 'SELECT (DQL)',
                            'items':[{
                                'label': 'SELECT',
                                'cmd': 'QUERY',
                                'args':{'query': 'SELECT'}
                            }, {
                                'label': 'SELECT ... ORDER BY',
                                'cmd': 'QUERY',
                                'args':{'query': 'SELECT_ORDER'}
                            }, {
                                'label': 'SELECT ... WHERE',
                                'cmd': 'QUERY',
                                'args':{'query': 'SELECT_WHERE'}
                            }, {
                                'label': 'SELECT ... GROUP BY',
                                'cmd': 'QUERY',
                                'args':{'query': 'SELECT_GROUP'}
                            }, {
                                'label': 'SELECT ... JOIN',
                                'cmd': 'QUERY',
                                'args':{'query': 'SELECT_JOIN'}
                            }]
                        }, {
                            'label': 'INSERT',
                            'cmd': 'QUERY',
                            'args':{'query': 'INSERT'}
                        }, {
                            'label': 'UPDATE',
                            'cmd': 'QUERY',
                            'args':{'query': 'UPDATE'}
                        }, {
                            'label': 'DELETE',
                            'cmd': 'QUERY',
                            'args':{'query': 'DELETE'}
                         }]
                    }, {
                        'id': 'DTL',
                        'label': 'DTL',
                        'items': [{
                            'label': 'BEGIN TRANSACTION',
                            'cmd': 'QUERY',
                            'args':{'query': 'BEGIN'}
                        }, {
                            'label': 'COMMIT',
                            'cmd': 'QUERY',
                            'args':{'query': 'COMMIT'}
                        }, {
                            'label': 'ROLLBACK',
                            'cmd': 'QUERY',
                            'args':{'query': 'ROLLBACK'}
                        }, {
                            'label': 'SAVEPOINT',
                            'cmd': 'QUERY',
                            'args':{'query': 'SAVEPOINT'}
                         }]
                    }, {
                        'label': '-'
                    }, {
                        'label': 'Run',
                        'cmd': 'RUN',
                        'key': 'F5'
                    }]
                }, {
                    'id': 'HELP',
                    'label':'Help',
                    'items': [{
                        'label':'SQL by SQLite...',
                        'cmd': 'HELP_SQLITE'
                    }, {
                        'label':'About...',
                        'cmd': 'ABOUT'
                    }]
                }],
        ## Main Content ---------------------------------------
                'content': [{
                ## Top Panel: ---------------------------------
                    'id': 'top_panel',
                    'type': Frame,
                    'side':'top',
                    'items':[{
                        'id': 'btn_db',
                        'type': Button,
                        'text': 'Open Data Base...',
                        'cmd': 'OPEN',
                        'grid': (1, 2)
                    }]
                }, {
                ## Table List Panel: ---------------------------------------
                    'id': 'pnl_tables',
                    'type': Frame,
                    'side': 'left',
                    'items': [{
                    ##---Table Button List Panel
                        'id': 'pnl_tbl_lst',
                        'type': Frame,
                        'side': 'top'
                    }, {
                    ##---Panel New Table
                        'id': 'pnl_new_table',
                        'type': Frame,
                        'side': 'bottom',
                        'items': [{
                            'id': 'btn_new_table',
                            'type': Button,
                            'text': 'New Table',
                            'cmd': 'QUERY',
                            'args':{'query': 'CREATE', 'table':None},
                            'grid': (1, 2)
                        }]
                    }]
                 }, {
                ## Query Panel: -----------------------------------
                    'id': 'pnl_query',
                    'type': Frame,
                    'expand':True,
                    'side': 'right',
                    'items': [{
                        'id':'pnd',
                        'type':PanedWindow,
                        'cfg':{'orient': VERTICAL, 'sashrelief': RIDGE, 'sashwidth': 3},
                        'side': 'top',
                        'expand': True,
                        'items':[{
                        ##---SQL Panel
                            'id': 'sql_panel',
                            'type': Frame,
                            'side': 'top',
                            'items': [{
                                'id': 'lbl_sql',
                                'type': Label,
                                'text': 'SQL:',
                                'grid': (1, 1)
                            }, {
                                'id': 'txt_sql',
                                'type': TextSQL,
                                'grid': (1, 2),
                                'dimension': (70, 10),
                                'signal':{'event':'F5', 'cmd':'RUN'}
                            }, {
                                'id': 'btn_run',
                                'type': Button,
                                'text': 'Run',
                                'cmd': 'RUN',
                                'grid': (1, 3)
                            }, {
                            ##----Buttons Panel
                                'id': 'btns_panel',
                                'type': Frame,
                                'grid': (2, 2),
                                'items': [{
                                    'id': 'btn_ins',
                                    'type': Button,
                                    'text': 'INSERT',
                                    'cmd': 'QUERY',
                                    'args':{'query': 'INSERT'},
                                    'grid': (1, 1),
                                    'dimension': (12, None)
                                }, {
                                    'id': 'btn_sel',
                                    'type': Button,
                                    'text': 'SELECT',
                                    'cmd': 'QUERY',
                                    'args':{'query': 'SELECT'},
                                    'grid': (1, 2),
                                    'dimension': (12, None)
                                }, {
                                    'id': 'btn_upd',
                                    'type': Button,
                                    'text': 'UPDATE',
                                    'cmd': 'QUERY',
                                    'args':{'query': 'UPDATE'},
                                    'grid': (1, 3),
                                    'dimension': (12, None)
                                }, {
                                    'id': 'btn_del',
                                    'type': Button,
                                    'text': 'DELETE',
                                    'cmd': 'QUERY',
                                    'args':{'query': 'DELETE'},
                                    'grid': (1, 4),
                                    'dimension': (12, None)
                                }]
                            }]
                        }, {
                        ##---Result Panel
                            'id': 'result_panel',
                            'type': Frame,
                            'side': 'top',
                            'expand': True,
                            'items': [{
                                'id': 'txt_res',
                                'type': TextResult,
                                'side': 'top',
                                'expand': True
                            }]
                        }]
                    }]
                }]
            }
        self.commands = {
            'NEW': self.newDataBaseFile,
            'OPEN': self.openDataBaseFile,
            'ABOUT': self.about,
            'QUERY': self.showQuery,
            'RUN': self.run,
            'TABLES_RL': self.showTablesAtRightOrLeft,
            'HELP_SQLITE': self.helpSqlSqlite,
            'SOURCE': self.viewSourceCode,
            'ERD': self.showERD
        }
        self.uiB = UIBuilder(self.win, self.uiCfg, self.commands)
        self.ui = self.uiB.createContent()
        self.app = app
        try:
            confDir = os.environ['HOME']
        except KeyError:
            confDir = os.environ['USERPROFILE']
        self.configFile = os.path.join(confDir, self.configFile)
        self.loadLastDataBaseFile()
        self.win.mainloop()

    def showERD(self):
        w = Toplevel(self.win)
        w.title(_("Entity Relationship Diagram"))
        c = DiagramCanvas(w)
        rels = []
        tables = [t for t in self.app.getTables() if t.typ != 'view']
        for t in tables:
            if str(t) != 'sqlite_sequence':
                c.addBlock(str(t))
                for fk in self.app.getForeignKeys(str(t)):
                    fk_table = fk[2]
                    rels.append((str(t), fk_table))
        for t1, t2 in rels:
            c.addArrow(c.blocks[t1], c.blocks[t2])
        c.pack()
        w.focus_set()

    def viewSourceCode(self):
        from idlelib.EditorWindow import EditorWindow, fixwordbreaks
        from idlelib import macosxSupport
        fixwordbreaks(self.win)
        macosxSupport.setupApp(self.win, None)
        edit = EditorWindow(root=self.win, filename='dbeditor.py')
        edit.text.bind("<<close-all-windows>>", edit.close_event)
        self.win.mainloop()

    def tableContextMenu(self, event, table=''):
        popup = Menu(self.win, tearoff=0)
        popupcfg = [{
            'id': 'ADD',
            'label':'Add Column',
            'cmd': 'QUERY',
            'args': {'query': 'ADD', 'table': table}
        }, {
            'id': 'RENAME',
            'label':'Rename Table',
            'cmd': 'QUERY',
            'args': {'query': 'RENAME', 'table': table}
        }, {
            'id': 'DROP',
            'label':'Drop Table',
            'cmd': 'QUERY',
            'args': {'query': 'DROP', 'table': table}
        }, {
            'label':'-',
        }, {
            'id': 'CLONE',
            'label':'Clone Table Structure',
            'cmd': 'QUERY',
            'args': {'query': 'CLONE', 'table': table}
        }, {
            'id': 'COPY',
            'label':'Copy Table Data',
            'cmd': 'QUERY',
            'args': {'query': 'COPY', 'table': table}
        }, {
            'label':'-',
        }, {
            'id': 'VIEW',
            'label':'Create View',
            'cmd': 'QUERY',
            'args': {'query': 'VIEW', 'table': table}
        }, {
            'id': 'TRIGGER',
            'label':'Create Trigger',
            'cmd': 'QUERY',
            'args': {'query': 'TRIGGER', 'table': table}
        }]
        self.uiB.createMenu(popup, popupcfg)
        popup.post(event.x_root, event.y_root)

    def showTables(self):
        tables = self.app.getTables()
        self.ui['pnl_tbl_lst'].destroy()
        self.ui['pnl_tbl_lst'] = Frame(self.ui['pnl_tables'])
        self.ui['pnl_tbl_lst'].pack(side="top", fill="both", pady=10)
        self.btnTables = {}
        Label(self.ui['pnl_tbl_lst'], text=_('Tables')).grid(row=0, column=1)
        selectedTableExists = False
        for n, table in enumerate(tables):
            btn = tkButton(self.ui['pnl_tbl_lst'], text=table, width=20,
                command=self.uiB.command(cmd='QUERY', query='SELECT_TABLE',
                        table=table))
            btn.grid(row=n + 1, column=1)
            btn.bind("<Button-3>",
                lambda event, table=table: self.tableContextMenu(event, table))
            self.btnTables[str(table)] = btn
            if str(table) == str(self.selectedTable):
                self.markTable(table)
                selectedTableExists = True
        if not selectedTableExists:
            self.selectedTable = None

    def helpSqlSqlite(self):
        webbrowser.open(self.sqlSqliteUrl)

    def about(self):
        showinfo(_('About...'), self.appName + '\n' + _('Desarrollado por:') +
            '\n' + self.author + '\n' + _('Agosto 2014'))

    def showTablesAtRightOrLeft(self):
        self.tablesAtLeft = not self.tablesAtLeft
        qSide, tSide = ("right", "left") if self.tablesAtLeft else (
                                ("left", "right"))
        self.ui['pnl_tables'].pack(side=tSide, fill="both", pady=10, padx=10)
        self.ui['pnl_query'].pack(side=qSide, fill="both", expand=True, padx=10)
        self.ui['VIEW'].entryconfigure(0, label=_("Tables at %s") % _(qSide))

    def selectTable(self, table):
        self.unMarkTable(self.selectedTable)
        self.selectedTable = table if table != '' else self.selectedTable
        self.markTable(self.selectedTable)
        return self.selectedTable

    def unMarkTable(self, table):
        try:            
            self.btnTables[str(table)].state(('!pressed',))
        except AttributeError:
            self.btnTables[str(table)].config(relief='raised')
        except KeyError:
            pass

    def markTable(self, table):
        try:
            self.btnTables[str(table)].state(('pressed',))
        except AttributeError:
            self.btnTables[str(table)].config(relief='sunken')
        except KeyError:
            pass

    def newDataBaseFile(self):
        path = asksaveasfilename(defaultextension=".db")
        if path != '':
            self.openDataBase(path)

    def openDataBaseFile(self):
        tipes = "*.db;*.dat;*.sqlite;*.sqlite3;*.sql;"
        tipes = ((_("Data base files"), tipes), (_("All files"), "*.*"))
        path = askopenfilename(filetypes=tipes)
        if path != '':
            self.openDataBase(path)

    def loadLastDataBaseFile(self):
        path = self.readPath()
        if path != '':
            self.openDataBase(path)

    def savePath(self, path):
        with open(self.configFile, 'w') as f:
            f.write(str(path))

    def readPath(self):
        try:
            with open(self.configFile, 'r') as f:
                return str(f.read())
        except IOError:
            return ''

    def openDataBase(self, path):
        self.basePath = path if path != '' else self.basePath
        try:
            self.app.openDataBase(self.basePath)
            self.savePath(self.basePath)
            self.ui['btn_db'].config(text=self.basePath)
            self.dbOpened = True
            self.selectedTable = None
            self.showTables()
        except IOError:
            showerror(_('Error'), _('Error') + ' ' + _("Open Data Base..."))

    def showQuery(self, query, table=''):
        if self.dbOpened:
            table = self.selectTable(table)
            sql = self.app.createQuery(query, table)
            self.ui['txt_sql'].setText(sql)
            if self.app.msg != '':
                self.ui['txt_res'].setText('')
        else:
            showerror(_('Error'), _('Error') + ' ' + _("Open Data Base..."))

    def run(self):
        query = self.ui['txt_sql'].getText()
        result = self.app.runQuery(query)
        self.ui['txt_res'].setText(result)
        self.showTables()


class Table:
    def __init__(self, name, typ='table'):
        self.name = name
        self.typ = typ

    def __str__(self):
        return ('<view> ' if self.typ == 'view' else '') + self.name


class DataBase:

    def __init__(self, path=''):
        if path != '':
            self.open(path)

    def open(self, path):
        self.affectedRows = 0
        self.transactStarted = False
        self.path = path
        try:
            self.conexion = connect(path, isolation_level=None)
            self.cursor = self.conexion.cursor()
            self.conexion.row_factory = Row
            self.executeQuery("PRAGMA foreign_keys = ON;")
        except IOError:
            print('Invalid Path')

    def getTables(self):
        sql = """SELECT name, type
                 FROM sqlite_master
                 WHERE type='table' OR type='view';"""
        res = self.executeQuery(sql)
        tables = []
        for tbl, typ in res:
            tables.append(Table(tbl, typ))
        return tables

    def getAffectedRows(self):
        return self.affectedRows

    def executeQuery(self, sql):
        self.transactStarted = self.transactStarted or sql.startswith('BEGIN')
        self.cursor.execute(sql)
        if not self.transactStarted:
            self.conexion.commit()
        else:
            self.transactStarted = not sql.startswith('ROLLBACK') and (
                not sql.startswith('COMMIT') and not sql.startswith('END'))
        self.affectedRows = self.cursor.rowcount
        return self.cursor.fetchall()

    def getColumns(self, table=''):
        try:
            if table != '':
                sql = 'SELECT * FROM ' + table.name
                self.executeQuery(sql)
            return list([x[0] for x in self.cursor.description])
        except:
            return []

    def getCreateStatement(self, table):
        sql = """SELECT sql
                 FROM sqlite_master
                 WHERE type='table' AND tbl_name='""" + table.name + "';"
        res = self.executeQuery(sql)
        return res[0][0]

    def getInfo(self, table):
        sql = "PRAGMA table_info(" + table + ");"
        return self.executeQuery(sql)

    def getForeignKeys(self, table):
        sql = "PRAGMA foreign_key_list(" + table + ");"
        return self.executeQuery(sql)


class QueryBrowser:
    db = None
    msg = ''

    def run(self):
        self.ui = QueryBrowserUI(self)
        #self.ui.start()

    def openDataBase(self, path):
        self.db = DataBase(path)

    def runQuery(self, query):
        if not query.strip():
            return '<<< {0}. >>>'.format(_('Empty SQL command'))
        try:
            rb = ResultBuilder()
            res = self.db.executeQuery(query)
            cols = self.db.getColumns()
            rows = self.db.getAffectedRows()
            msg = ', %d %s' % (rows, _('rows affected')) if rows > 0 else ''
            self.msg = '' if len(cols) > 0 else (
                ' <<< %s%s. >>> ' % (_('Command executed successfully'), msg))
            return rb.getGrid(cols, res, self.msg)
        except:
            self.msg = ' <<< %s!!! %s %s >>> ' % (
                _("Error").upper(),
                sys.exc_info()[0].__name__, sys.exc_info()[1])
            return self.msg

    def getTables(self):
        return self.db.getTables()

    def getForeignKeys(self, table):
        return self.db.getForeignKeys(table)

    def createQuery(self, query, table=None):
        qb = QueryBuilder()
        if query == 'CLONE':
            if table.typ != 'view':
                return self.db.getCreateStatement(table).replace(
                    table.name, table.name + '_copy', 1)
            else:
                return qb.ERROR_VIEW % 'CLONE'
        else:
            columns = self.db.getColumns(table) if table != '' else []
            return qb.getQuery(statement=query, table=table, columns=columns)


class ResultBuilder:
    def __init__(self):
        self.colWidts = []

    def getGrid(self, columns, result, msg=''):
        if columns == []:
            return msg
        self.appendRow(columns)
        for row in result:
            self.appendRow(row)
        text = self.getRow(columns, header=True)
        for row in result:
            text += self.getRow(row)
        return text

    def getRow(self, rowValues, header=False):
        text = ''
        fCell = lambda v, n: (' ' + str(v)).ljust(n + 2)
        valWidts = list(zip(rowValues, self.colWidts))
        data = [fCell('NULL' if v is None else v, n) for v, n in valWidts]
        if header:
            lines = ['═' * (w + 2) for w in self.colWidts]
            text += "╔" + ('╤'.join(lines)) + "╗\n"
            text += "║" + ('│'.join(data)) + "║\n"
            text += "╠" + ('╪'.join(lines)) + "╣\n"
        else:
            lines = ['─' * (w + 2) for w in self.colWidts]
            text = "║" + ('│'.join(data).replace("\n", ' ')) + "║\n"
            text += "╟" + ('┼'.join(lines)) + "╢\n"
        return text

    def appendRow(self, rowValues):
        if self.colWidts == []:
            self.colWidts = [len(x) for x in rowValues]
        else:
            valWidts = list(zip(rowValues, self.colWidts))
            self.colWidts = [max(len(str(x)), y) for x, y in valWidts]


class QueryBuilder:
    ERROR_VIEW = '/*You can not {action} to a VIEW*/'

    def getQuery(self, statement, **args):
        commands = {
            #DDL
            'CREATE': self.create,
            'ADD': self.add,
            'RENAME': self.rename,
            'DROP': self.drop,
            'VIEW': self.view,
            'TRIGGER': self.trigger,
            #DML
            'INSERT': self.insert,
            'COPY': self.copy,  # INSERT ... SELECT
            'UPDATE': self.update,
            'DELETE': self.delete,
            #DQL
            'SELECT': self.select,
            'SELECT_TABLE': self.select_table,
            'SELECT_ORDER': self.select_order,
            'SELECT_WHERE': self.select_where,
            'SELECT_GROUP': self.select_group,
            'SELECT_JOIN':  self.select_join,
            #DTL
            'BEGIN': self.begin,
            'COMMIT': self.commit,
            'ROLLBACK': self.rollback,
            'SAVEPOINT': self.savepoint
        }
        try:
            return commands[statement.upper()](**args)
        except:
            msg = '/* <<< %s!!! %s %s >>> */' % (
                _("Error").upper(),
                sys.exc_info()[0].__name__, sys.exc_info()[1])
            return msg

    def build(self, sql, table=None, columns=[], allowView=False, allCols='*'):
        table = table if table is not None else Table('<table>')
        cols = ', '.join(columns) if columns != [] else allCols
        vals = ["  '<data>' /*{0}*/".format(col) for col in columns]
        vals = '\n' + ', \n'.join(vals) + '\n' if len(vals) > 0 else '<values>'
        sql = sql.format(table=table.name, columns=cols, values=vals)
        action = sql.split()[0]
        err = self.ERROR_VIEW.format(action=action)
        return sql if table.typ != 'view' or allowView else err

    #DDL
    def create(self, table, columns):
        sql = "CREATE TABLE {table} (\n"
        sql += " /*FIELDS*/\n"
        sql += "  <field> INTEGER /*PRIMARY KEY AUTOINCREMENT*/,\n"
        sql += "  <field> TEXT NOT NULL /*REFERENCES <table>(<field>)*/,\n"
        sql += "  <field> REAL UNIQUE,\n"
        sql += "  <field> BLOB DEFAULT <value>,\n"
        sql += " /*CONSTRAINTS*/\n"
        sql += "  PRIMARY KEY(<field>, <field>),\n"
        sql += "  FOREIGN KEY(<field>) REFERENCES <table>(<field>)\n"
        sql += ");"
        return self.build(sql, table, allowView=True)

    def add(self, table, columns):
        sql = "ALTER TABLE {table}\n"
        sql += "ADD COLUMN "
        sql += "<field> <INTEGER|REAL|TEXT|BLOB> \n"
        sql += "/*PRIMARY KEY|NOT NULL|UNIQUE|DEFAULT|REFERENCES*/;"
        return self.build(sql, table)

    def rename(self, table, columns):
        sql = "ALTER TABLE {table}\n"
        sql += "RENAME TO <new_table_name>;"
        return self.build(sql, table)

    def drop(self, table, columns):
        sql = 'DROP {typ} {name};'
        typ = '<TABLE|VIEW|TRIGGER>' if table is None else table.typ.upper()
        name = table.name if table is not None else '<name>'
        sql = sql.format(name=name, typ=typ)
        return sql

    def view(self, table, columns):
        sql = 'CREATE VIEW <name> AS\n'
        sql += 'SELECT {columns}\n'
        sql += 'FROM {table};'
        return self.build(sql, table, columns, allowView=True)

    def trigger(self, table, columns):
        sql = 'CREATE TRIGGER <name> \n'
        sql += '<AFTER|BEFORE> <UPDATE|INSERT|DELETE> '
        sql += '/*OF <field>*/ ON {table}\n'
        sql += 'BEGIN \n'
        sql += '  <sql OLD, NEW> \n'
        sql += 'END; '
        return self.build(sql, table)

    #DQL
    def select(self, table, columns):
        sql = 'SELECT {columns}\n'
        sql += 'FROM {table}\n'
        sql += 'WHERE <condition>\n'
        sql += 'GROUP BY <field>\n'
        sql += '  HAVING <condition>\n'
        sql += 'ORDER BY <field>\n'
        sql += 'LIMIT <value>, <value>;'
        return self.build(sql, table, columns, allowView=True)

    def select_table(self, table, columns):
        sql = 'SELECT {columns}\n'
        sql += 'FROM {table};'
        return self.build(sql, table, columns, allowView=True)

    def select_order(self, table, columns):
        sql = 'SELECT {columns}\n'
        sql += 'FROM {table}\n'
        sql += 'ORDER BY <field>;'
        return self.build(sql, table, columns, allowView=True)

    def select_where(self, table, columns):
        sql = 'SELECT {columns}\n'
        sql += 'FROM {table}\n'
        sql += 'WHERE <condition (IS|IN|LIKE|BETWEEN)>;'
        return self.build(sql, table, columns, allowView=True)

    def select_group(self, table, columns):
        sql = 'SELECT <fields (COUNT|SUM|MIN|MAX|AVERAGE)> AS <alias> \n'
        sql += 'FROM {table}\n'
        sql += 'GROUP BY <field>\n'
        sql += '  HAVING <condition>;'
        return self.build(sql, table, columns, allowView=True)

    def select_join(self, table, columns):
        sql = 'SELECT <alias.field>\n'
        sql += 'FROM {table} AS <alias>\n'
        sql += '<INNER|LEFT|RIGHT|OUTER> JOIN <table> AS <alias>\n'
        sql += '    ON <condition>;'
        return self.build(sql, table, columns, allowView=True)

    #DML
    def insert(self, table, columns):
        sql = "INSERT INTO {table} ({columns})\n"
        sql += "VALUES ({values})"
        return self.build(sql, table, columns, allCols='<columns>')

    def copy(self, table, columns):
        sql = 'INSERT INTO <table>\n'
        sql += 'SELECT {columns}\n'
        sql += 'FROM {table};'
        return self.build(sql, table, columns, allowView=True)

    def update(self, table, columns):
        sql = 'UPDATE {table}\n'
        sql += "SET <field> = '<value>'\n"
        sql += "WHERE  <field> = '<value>';"
        return self.build(sql, table)

    def delete(self, table, columns):
        sql = "DELETE FROM {table}\n"
        sql += "WHERE <field> = '<value>';"
        return self.build(sql, table)

    #DTL
    def begin(self, table, columns):
        sql = 'BEGIN TRANSACTION;'
        return sql

    def commit(self, table, columns):
        sql = 'COMMIT;'
        return sql

    def rollback(self, table, columns):
        sql = 'ROLLBACK /*TO <savepoint>*/;'
        return sql

    def savepoint(self, table, columns):
        sql = 'SAVEPOINT <name>;'
        return sql

if __name__ == '__main__':
    app = QueryBrowser()
    app.run()
