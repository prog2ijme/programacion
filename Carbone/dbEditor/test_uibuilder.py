from tkinter import Tk, Button, Frame, Label, PanedWindow, HORIZONTAL, RIDGE
from dbeditor import UIBuilder


def holaMundo():
    print('hola mundo')


class UiDesigner(UIBuilder):

    def __init__(self, *args):
        super().__init__(*args)
        self.win.bind_class("mytag", '<Button-1>', self.highlight)
        self.selectedWidget = None
        self.selectedOrigBg = None

    def highlight(self, event):
        if self.selectedWidget is not None:
            self.selectedWidget.config(background=self.selectedOrigBg)
        self.selectedWidget = event.widget
        self.selectedOrigBg = self.selectedWidget.cget('bg')
        self.selectedWidget.config(background='#A87')

    def createContentItem(self, parent, item):
        super().createContentItem(parent, item)
        e = self.ui[item['id']]
        e.bindtags(e.bindtags() + ("mytag",))


t = Tk()
a = UiDesigner(t)
cfg = [{
            'id': 'ARCHIVO',
            'label': 'Archivo',
            'items': [{
                'id': 'HOLA',
                'label': 'Hola',
                'cmd': 'HOLA'
            }, {
                'id': 'EXIT',
                'label': 'Salir',
                'cmd': 'EXIT'
             }]
        }]

a.createMainMenu(cfg)
a.createContent(config=[{
    'id':'pnd',
    'type':PanedWindow,
    'cfg':{'orient': HORIZONTAL, 'sashrelief': RIDGE, 'sashwidth': 3},
    'side': 'top',
    'expand': True,
    'items':[{
        'id': 'b1',
        'type': Button,
        'text': 'Hola'
    }, {
        'id': 'b2',
        'type': Button,
        'text': 'Mundo',
        'cmd': 'HOLA'
    }]
}])

a.bindCommands({'HOLA': holaMundo})

a.start()
