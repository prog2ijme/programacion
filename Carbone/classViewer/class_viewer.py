
from tkinter import Tk, Menu
import os
from diagram import DiagramCanvas
import ast
from tkinter.filedialog import askopenfilename  # , askdirectory


class Class:

    def __init__(self, nombre):
        self.nombre = nombre
        self.x = 0
        self.y = 0
        self.parents = []
        self.lineaCodigo = 0
        self.level = 0
        padx = 20
        pady = 5
        self.width = len(nombre) * 10 + 2 * padx
        self.height = 18 + 2 * pady


class ClassViewer(Tk):
    '''Illustrate how to drag items on a Tkinter canvas'''

    def __init__(self, *args, **kwargs):
        Tk.__init__(self, *args, **kwargs)
        menu = Menu(self)
        self.config(menu=menu)
        menu.add_command(label='Open Python File', command=self.openProject)
        menu.add_command(label='Run Python File', command=self.runProject)
        self.pos = {}

        # create a canvas
        self.canvas = DiagramCanvas(self, width=800, height=600)
        self.canvas.bind('<Enter>', self.OnEnter)
        self.canvas.pack(fill="both", expand=True)
        self.lastTimeLoad = None
        self.module = None
        self.edit = None

    def runProject(self):
        code = open(self.filePath, encoding="utf8").read()
        code = compile(code, self.filePath, 'exec')
        #print(globals())
##        variables['__file__'] = self.filePath
        variables = {
            '__file__': self.filePath,
            '__name__': '__main__',
            '__builtins__': globals()['__builtins__']}
##        print(variables)
        exec(code, variables)

    def openProject(self):
        # create a couple movable objects
        #dir_path = askdirectory()
        #dir_path = ''
        #moduleName = ""
        #self.filePath = dir_path + "/" + moduleName + ".py"
        tipes = [("Python File", "*.py")]
        self.filePath = askopenfilename(filetypes=tipes)
        if self.filePath != '':
            self.createElements()

    def createElements(self):
        # this data is used to keep track of an
        # item being dragged
        self.line = {}
        self.part = {}
        self.items = {}
        self.parents = {}
        self.childs = {}
        self.filenums = {}
        for k in self.canvas.blocks:
            b = self.canvas.blocks[k]
            self.pos[k] = (b.x, b.y)
        self.canvas.clear()
        self.loadClasses(self.filePath)

    def loadClasses(self, filePath):
        f = open(filePath, 'r', encoding="utf8")
        cod = f.read()
        f.close()
        self.module = ast.parse(cod)
        classes = [(node.name, node)
            for node in ast.walk(self.module)
            if isinstance(node, ast.ClassDef)]

        self.lastTimeChange = os.stat(filePath).st_mtime

        self.classBlocks = {}

        for classEl in classes:
            clsName = classEl[0]
            x, y = self.pos[clsName] if clsName in self.pos else (0, 0)
            self.classBlocks[clsName] = self.canvas.addBlock(clsName, x, y)
            self.filenums[clsName] = (filePath, classEl[1].lineno)

        for classEl in classes:
            className = classEl[0]
            for cls in classEl[1].bases:
                superClassName = cls.id
                if superClassName in self.classBlocks:
                    b1 = self.classBlocks[className]
                    b2 = self.classBlocks[superClassName]
                    self.canvas.addArrow(b1, b2)

        self.canvas.tag_bind("block", "<Double-Button-1>", self.OnDblClick)

    def OnEnter(self, ev):
        self.updateGraph()

    def OnDblClick(self, event):
        line = 0
        tok = self.canvas.find_closest(event.x, event.y)[0]
        className = self.canvas.getBlock(tok).text
        if className in self.filenums:
            path, line = self.filenums[className]

        if self.edit is None or self.edit.text is None:
            from idlelib.EditorWindow import EditorWindow, fixwordbreaks
            from idlelib import macosxSupport
            fixwordbreaks(self)
            macosxSupport.setupApp(self, None)
            self.edit = EditorWindow(root=self, filename=path)
            self.edit.text.bind("<<close-all-windows>>", self.edit.close_event)

        self.edit.gotoline(line)
        self.edit.text.focus_set()

        self.mainloop()

    def updateGraph(self):
        try:
            if os.stat(self.filePath).st_mtime != self.lastTimeChange:
                self.createElements()
        except:
            pass

if __name__ == "__main__":
    app = ClassViewer()
    app.mainloop()
