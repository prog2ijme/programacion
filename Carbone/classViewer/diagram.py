from tkinter import Canvas, Tk, LAST


class DiagramCanvas(Canvas):

    def __init__(self, parent, width=800, height=600):
        super(DiagramCanvas, self).__init__(parent)
        self.config(width=width, height=height, bg='#F9F6EA')
        self.width = width
        self.height = height
        self.items = {}
        self.arrows = {}
        self.blocks = {}
        self.margin = 20
        self.spacing = 80
        self._drag_data = {}
        self.tag_bind("block", "<ButtonPress-1>", self.onDragStart)
        self.tag_bind("block", "<ButtonRelease-1>", self.onDragEnd)
        self.tag_bind("block", "<B1-Motion>", self.onDrag)

    def clear(self):
        self.delete("all")
        self.blocks = {}
        self.items = {}
        self.arrows = {}

    def bestPlace(self, b):
        c = self.width / 2
        m = self.height / 2 - self.spacing
        bw = b.width
        bh = b.height
        sp = self.spacing
        places = [
            (c, m),
            (c, m - (bh + sp)),
            (c - (bw + sp), m - (bh + sp)),
            (c - (bw + sp), m),
            (c + (bw + sp), m - (bh + sp)),
            (c + (bw + sp), m),
            (c, m + (bh + sp)),
            (c - (bw + sp), m + (bh + sp)),
            (c + (bw + sp), m + (bh + sp))
        ]
        if len(self.blocks) < len(places):
            return places[len(self.blocks)]
        else:
            x, y = places[len(self.blocks) - len(places)]
            return (x, y + m + bh + sp)

    def addBlock(self, text, x=0, y=0):
        b = Block(text, x, y)
        if (x, y) == (0, 0):
            (x, y) = self.bestPlace(b)
            b.x = x
            b.y = y
        b.create(self)
        self.items.update(b.items())
        #self.items[b.id] = b
        #self.items[b.textid] = b
        self.blocks[text] = b
        return b

    def addArrow(self, blockFrom, blockTo, where=None):
        where1, where2 = where if where is not None else (None, None)
        x1, y1 = blockFrom.faceTo(blockTo, where1)
        x2, y2 = blockTo.faceTo(blockFrom, where2)
        shape = (12, 12, 8)
        idArrow = self.create_line(x1, y1, x2, y2, arrow=LAST, arrowshape=shape)
        self.arrows[idArrow] = (blockFrom, blockTo, where1, where2)

    def onDragEnd(self, event):
        '''End drag of an object'''
        # reset the drag information
        self._drag_data["item"] = None
        self._drag_data["x"] = 0
        self._drag_data["y"] = 0

    def onDrag(self, event):
        '''Handle dragging of an object'''
        # compute how much this object has moved
        delta_x = event.x - self._drag_data["x"]
        delta_y = event.y - self._drag_data["y"]
        # move the object the appropriate amount
        item = self._drag_data["item"]
        item.x += delta_x
        item.y += delta_y
        for i in item.ids():
            self.move(i, delta_x, delta_y)
        # record the new position
        self._drag_data["x"] = event.x
        self._drag_data["y"] = event.y
        for idArrow in self.arrows:
            b1, b2, where1, where2 = self.arrows[idArrow]
            x1, y1 = b1.faceTo(b2, where1)
            x2, y2 = b2.faceTo(b1, where2)
            self.coords(idArrow, x1, y1, x2, y2)

    def getBlock(self, idItem):
        return self.items[idItem]

    def onDragStart(self, event):
        '''Being drag of an object'''
        # record the item and its location
        idItem = self.find_closest(event.x, event.y)[0]
        self._drag_data["item"] = self.getBlock(idItem)
        self._drag_data["x"] = event.x
        self._drag_data["y"] = event.y


class Block:

    def __init__(self, text, x=0, y=0, color="#F5D3AC", padx=20, pady=5):
        self.id = None
        self.textid = None
        self.text = text
        self.color = color
        self.padx = padx
        self.pady = pady
        self.width = len(text) * 10 + 2 * self.padx
        self.height = 18 + 2 * self.pady
        self.x = x
        self.y = y
        self.jointo = []
        self.font = "Courier 10 bold"
        self.borderColor = "black"

    def create(self, canvas):
        self.textid = canvas.create_text(self.x, self.y, text=self.text,
            tags="block", font=self.font)
        bounds = canvas.bbox(self.textid)
        tw = bounds[2] - bounds[0]
        th = bounds[3] - bounds[1]
        self.width = tw + 2 * self.padx
        self.height = th + 2 * self.pady
        self.id = canvas.create_rectangle(
            self.left(), self.top(), self.right(), self.bottom(),
            outline=self.borderColor, fill=self.color, tags="block")
        canvas.tag_raise(self.textid)

    def items(self):
        return {key: self for key in self.ids()}

    def ids(self):
        return [self.id, self.textid]

    def left(self):
        return self.x - (self.width / 2)

    def top(self):
        return self.y - (self.height / 2)

    def right(self):
        return self.x + (self.width / 2)

    def bottom(self):
        return self.y + (self.height / 2)

    def pos(self, where):
        hor, ver = where
        x = self.right() if hor == 'r' else (
            self.left() if hor == 'l' else self.x)
        y = self.top() if ver == 't' else (
            self.bottom() if ver == 'b' else self.y)
        return (x, y)

    def faceTo(self, otherBlock, where=None):
        if where is not None:
            return self.pos(where)
        else:
            wy = 't' if self.top() > otherBlock.bottom() else (
                'b' if self.bottom() < otherBlock.top() else 'm')
            wx = 'c' if wy != 'm' else (
                'l' if self.left() > otherBlock.right() else (
                'r' if self.right() < otherBlock.left() else 'c'))
            despx = -10 if self.x - 10 > otherBlock.x else (
                     10 if self.x + 10 < otherBlock.x else 0)
            x, y = self.pos(wx + wy)
            return (x + despx, y) if wx == 'c' else (x, y)

if __name__ == '__main__':
    r = Tk()
    c = DiagramCanvas(r)
    b1 = c.addBlock('Hola')
    b2 = c.addBlock('Hola')
    c.addArrow(b1, b2, ('ct', 'cb'))
    c.addBlock('Hola rr')
    c.addBlock('Hola asdasd')
    c.addBlock('Hola')
    c.addBlock('Hola asdasd')
    c.addBlock('Hola')
    b9 = c.addBlock('Hola')
    c.addBlock('Hola Mundo')
    c.addArrow(b9, b1)
    c.addBlock('Hola asdasd asdasd asdasd')
    c.addBlock('Hola')
    c.addBlock('Hola')
    c.pack(fill="both", expand=True)
    r.mainloop()
