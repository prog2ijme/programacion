from tkinter import *
from random import randint

class Tetris:
    def __init__(self):
        self.ventana = Tk()
        self.ventana.title('Tetris')
        self.panel = Canvas(self.ventana, width=Bloque.ancho*10, height=Bloque.ancho*20, background='#AAA')
        self.panel.pack()
        self.bloques = []
        self.perder = False
        self.asignarEventos()
        
    def asignarEventos(self):
        self.ventana.bind("<Left>", self.moverIzquierda)
        self.ventana.bind("<Right>", self.moverDerecha)
        self.ventana.bind("<Down>", self.moverAbajo)
        self.ventana.bind("<Up>", self.rotar)
        self.ventana.after(1000, self.bajar)

    def iniciar(self):
        self.dibujarPieza(5, 1)
        self.ventana.mainloop()

    def dibujarPieza(self, x, y):
        self.pieza = self.obtererNuevaPieza(x, y)
        for b in self.pieza.bloques:
            self.dibujarBloque(b)
        if self.tieneBloquesAbajo(self.pieza):
            self.perder = True
                
    def obtererNuevaPieza(self, x, y):
        p = None
        r = randint(1,3)
        if r == 1:
            p = O(x,y)
        elif r == 2:
            p = T(x,y)
        elif r == 3:
            p = I(x,y)
        return p
        
    def moverPieza(self, dx, dy):
        if self.puedeMoverPieza(dx, dy):
            self.pieza.mover(dx,dy)
            for b in self.pieza.bloques:
                self.moverBloque(b, dx, dy)
        if self.estaTocandoFondo(self.pieza):
            self.fijarBloques()
            
    def puedeMoverPieza(self, dx, dy):
        if self.pieza.puedeMoverse(dx, dy):
            for b in self.pieza.bloques:
                if self.hayUnBloqueEn(b.x+dx, b.y+dy):
                    return False
            return True
        else:
             return False
            
    def puedeRotarPieza(self):
        for b in self.pieza.bloques:
            dx = self.pieza.x - (b.y - self.pieza.y) - b.x
            dy = self.pieza.y + (b.x - self.pieza.x) - b.y            
            if self.hayUnBloqueEn(b.x+dx, b.y+dy) or not b.puedeMoverse(dx, dy):
                return False
        return True
            
    def estaTocandoFondo(self, unaPieza):
        return unaPieza.hayUnBloqueEn(19) or self.tieneBloquesAbajo(unaPieza)
        
    def tieneBloquesAbajo(self, unaPieza):
        for b in unaPieza.bloques:
            if self.hayUnBloqueEn(b.x, b.y+1):
                return True
        return False
         
    def hayUnBloqueEn(self, x, y):
        return any(b.estaEn(x,y) for b in self.bloques)
        
    def linea(self, y):
        return [b for b in self.bloques if b.y == y]
        
    def limpiarLineasCompletas(self):
        for i in range(20):
            if len(self.linea(i)) == 10:
                self.limpiarLinea(i)
                
    def limpiarLinea(self, nro):
        for b in self.linea(nro):            
            self.eliminarBloque(b)
        for l in reversed(range(nro)):
            self.bajarLinea(l)

    def bajarLinea(self, nro):
        for b in self.linea(nro):
            self.moverBloque(b, 0, 1)
        
    def fijarBloques(self):
        self.bloques.extend(self.pieza.bloques)
        self.limpiarLineasCompletas()
        self.dibujarPieza(5, 1)
    
    def rotarPieza(self):
        if self.puedeRotarPieza():
            for b in self.pieza.bloques:
                self.rotarBloque(b, self.pieza.x, self.pieza.y)

    def rotarBloque(self, unBloque, ejex, ejey):
        dx = ejex - (unBloque.y - ejey) - unBloque.x
        dy = ejey + (unBloque.x - ejex) - unBloque.y
        self.moverBloque(unBloque, dx, dy)

    def moverBloque(self, unBloque, dx, dy):
        unBloque.mover(dx, dy)
        self.panel.move(unBloque.id, dx * Bloque.ancho, dy * Bloque.ancho)

    def dibujarBloque(self, bloque):
        px = bloque.x *Bloque.ancho
        py = bloque.y *Bloque.ancho
        bloque.id = self.panel.create_rectangle(px, py, px+Bloque.ancho,py+Bloque.ancho, fill=bloque.color)
        
    def eliminarBloque(self, unBloque):
        self.panel.delete(unBloque.id)
        self.bloques.remove(unBloque)
        
    def rotar(self, ev):
        self.rotarPieza()

    def moverDerecha(self, ev):
        self.moverPieza(1, 0)

    def moverIzquierda(self, ev):
        self.moverPieza(-1, 0)

    def moverAbajo(self, ev):
        self.moverPieza(0, 1)

    def bajar(self):
        if not self.perder:
            self.moverPieza(0, 1)
            self.ventana.after(1000, self.bajar)
        
class Bloque:
    ancho = 20
    def __init__(self, unColor, x, y):
        self.x = x
        self.y = y
        self.color = unColor

    def mover(self, dx,dy):
        self.x += dx
        self.y += dy
        
    def puedeMoverse(self, dx, dy):
        return self.x + dx >= 0 and self.x + dx < 10
        
    def estaEn(self, x, y):
        return self.x == x and self.y == y

class Pieza:

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.crearBloques()

    def mover(self, dx, dy):
        self.x += dx
        self.y += dy
            
    def hayUnBloqueEn(self, y):
        return any(b.y == y for b in self.bloques)
    
    def puedeMoverse(self, dx, dy):
        return all(b.puedeMoverse(dx,dy) for b in self.bloques)
        

class I(Pieza):

    def crearBloques(self):
        self.color = '#888'#'yellow'
        self.bloques = [
            Bloque(self.color, self.x, self.y - 1),
            Bloque(self.color, self.x, self.y),
            Bloque(self.color, self.x, self.y + 1),
            Bloque(self.color, self.x, self.y + 2)
        ]

class T(Pieza):

    def crearBloques(self):
        self.color = '#888' #'red'
        self.bloques = [
            Bloque(self.color, self.x - 1, self.y),
            Bloque(self.color, self.x,     self.y),
            Bloque(self.color, self.x + 1, self.y),
            Bloque(self.color, self.x,     self.y + 1)
        ]

class O(Pieza):

    def crearBloques(self):
        self.color = '#888'#'lightblue'
        self.bloques = [
            Bloque(self.color, self.x - 1, self.y),
            Bloque(self.color, self.x,     self.y),
            Bloque(self.color, self.x - 1, self.y + 1),
            Bloque(self.color, self.x,     self.y + 1)
        ]

t = Tetris()
t.iniciar()