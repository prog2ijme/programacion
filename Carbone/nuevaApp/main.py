import sys, os
from PyQt4.QtGui import QMainWindow, QApplication, QMessageBox
from PyQt4 import Qsci, QtCore, QtGui, uic

class App(QApplication):
    nombre = 'Mi app'
    autor = 'Yo mismo'
    
    def __init__(self, args):
        super().__init__(args, True)
        self.ventana = QMainWindow()
        self.path = os.path.dirname(os.path.realpath(args[0]))
        self.ui = uic.loadUi(self.path+"/main.ui", self.ventana)
        
    def iniciar(self):
        self.conectarEventos()
        self.ventana.show()
        status = self.exec_()
        sys.exit(status)
        
    def conectarEventos(self):
        self.lastWindowClosed.connect(self.quit)
        self.ui.actionAcerca_de.triggered.connect(self.acercaDe)
        self.ui.pushButton.clicked.connect(self.holaMundo)
        
    def holaMundo(self):
        QMessageBox.critical(self.ventana,"ALERTA","HOLA MUNDO")
        
    def acercaDe(self):
        QMessageBox.about(self.ventana, 'Acerca de... '+self.nombre, 'Desarrollado por... '+self.autor) 

def main(args):
    app = App(args)
    app.iniciar()
   
if __name__ == '__main__':
    main(sys.argv)
