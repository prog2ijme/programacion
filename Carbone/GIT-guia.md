
# Guia de básica de uso del sistema de control de versiones distribuido GIT: #
-----------------------------------------------------------------------------


## 1. Descarga e instalación: ##

 - En Windows: http://msysgit.github.io/
 - En Linux: http://git-scm.com/download/linux
 - En Mac: http://sourceforge.net/projects/git-osx-installer/

  Contiene:
 
   - Versión con interfaz gráfica (**Git GUI**)
   - Versión por linea de comandos (**Git Bash**)

----------------------------------

## 2. *Init*: Crear un repositorio nuevo: ##

 - ** GUI: **
 
    1. Seleccionar *"Create New Repository"*.
    2. Seleccionar el directorio donde crearlo.
  
  ---
  
 - ** Bash: **
 
    Dentro del directorio donde queremos crear el repositorio:
 
		$ git init
	
----------------------------------

## 3. *Clone*: Clonar un repositorio existente: ##

 - ** GUI: **
 
    1. Seleccionar "Clone Existing Repository"
    2. Insertar la URL del repositorio remoto 
	   (https://[NOMBRE_USUARIO]@bitbucket.org/prog2ijme/programacion.git **(*1)**)
    3. Seleccionar el directorio donde descargarlo
 
  ---

 - ** Bash: ** 
 
    Dentro del directorio donde quermos descargar el repositorio:
 
		$ git clone URL_DEL_REPOSITORIO_REMOTO

----------------------------------
 
## 4. *Commit*: Agregar los cambios realizados: ##

 - ** GUI: ** 
 
    1. Seleccionar los cambios desde "Unstaged" (Sección Naranja) 
		para agregarlos a "Staged"(Sección Verde) ('Staged' significa 'Pendientes de Confirmar')
    2. Clik en "Stage Changed" para agregar todos los cambios
		o bien "Commit">"Stage to Commit (Ctrl+T)" para agregar sólo los seleccionados
    3. Completar el campo "Commit Message" con un mensaje **breve y claro** de los cambios introducidos
    4. Click en Commit para confirmar los cambios "Staged"
  
  ---
  
 - ** Bash: **

    Dentro del directorio del repositorio:
 
		$ git add NOMBRE_ARCHIVO_A_AGREGAR_AL_STAGE #(Para agregar un archivo)
		$ git add -A  #(Para agregar todos los cambios)
		$ git commit -m "MENSAJE" 

----------------------------------
 
## 5. *Push*: Enviar los cambios a un repositorio remoto: ##

 - ** GUI: **
 
    1. Hacer click en "Push" o bien "Remote">"Push...(Ctrl+P)"
    2. Ingresar el usuario y/o la contraseña correspondiente
  
  ---

 - ** Bash: **
 
    Dentro del directorio del repositorio:
  
		$ git push
    Ingresar luego el usuario y/o la contraseña correspondiente

----------------------------------
 
## 6. *Fetch*: Traer cambios desde un repositorio remoto: ##

 - ** GUI: **
 
    1. Seleccionar *"Remote" > "Fetch from" > "origin"*
  
  ---
	 
 - ** Bash: **
 
    Dentro del directorio del repositorio:
  
		$ git fetch

----------------------------------
 
## 7. *Merge*: Mezclar cambios: ##

 - ** GUI: **
 
    1. Seleccionar *"Merge" > "Local Merge (Ctrl+M)"*
    2. Seleccionar la rama *"origin/master"* y click en *"Merge"*
  
  ---
	 
 - ** Bash: ** 
 
    Dentro del directorio del repositorio:
  
		$ git merge origin/master

------------------------------------------------------------
  **(*1)** Para usar su nombre de usuario, ingresar en https://bitbucket.org/ 
        usando la cuenta de google provista por el instituto 
		(luego agregar una contraseña para poder hacer '*push*').
		Alternativamente se puede usar **ijme** (pass:**ijme2014**)
		
  * * *